#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 15:19:06 2019

@author: gossn
"""

from netCDF4 import Dataset
from scipy.interpolate import griddata
import numpy as np
import os
import matplotlib.pyplot as plt
import math
from statsmodels import robust
import warnings
import time

#%% List available images

path0 = os.path.realpath('')
pathImg = '/home/gossn/Desktop/Link to Docencia_OC/platagusSpmWorkflow/imagery'
NSEW = [-33.96,-36.55,-54.70,-58.64]
#%% List available images

imgList = []
imgList = [pathImg + '/' + file for file in os.listdir(pathImg) if file.endswith('RhoS')]
imgList.append('') # Para que nunca quede de un solo archivo y no sepa como iterar en el paso siguiente
#%% Processor!
#%% Crop using NSEW
for img in imgList[:-1]:
	latLonNc4 = Dataset(img + '/geo_coordinates.nc','r')
	lat = latLonNc4.variables['latitude'][:,:]
	lon = latLonNc4.variables['longitude'][:,:]
	
	[R0,C0] = np.shape(lat)
	
	roi0 = np.where((lat<NSEW[0])&(lat>NSEW[1])&(lon<NSEW[2])&(lon>NSEW[3]))
	try:
		[rmin,rmax] = [np.max([np.min(roi0[0])-2,0]),np.min([np.max(roi0[0])+2,R0])]
		[cmin,cmax] = [np.max([np.min(roi0[1])-2,0]),np.min([np.max(roi0[1])+2,C0])]
		if (rmin==0)|(rmax==R0)|(cmin==0)|(cmax==C0):
			warnings.warn('ROI falls partially out of scene')
	except:
		print('ROI falls out of current scene!')
		break
	
	[R,C] = [rmax-rmin+1,cmax-cmin+1]
	lat = lat[rmin:(rmax+1),cmin:(cmax+1)]
	lon = lon[rmin:(rmax+1),cmin:(cmax+1)]
	#%% Read thuillier, Lat Lon, SunZen and interpolate from tie_points
	
	f0thuillier = np.loadtxt(path0 + '/OLCI_ancillary/olci_thuillier')[:,1]
	
	latLonTieNc4 = Dataset(img + '/tie_geo_coordinates.nc','r')
	latTie = latLonTieNc4.variables['latitude'][:,:]
	lonTie = latLonTieNc4.variables['longitude'][:,:]
	
	sunSenTieNc4= Dataset(img + '/tie_geometries.nc','r')
	sunSenTie = {}
	sunSen = {}
	sunSenStr = ['sza','saa','oza','oaa']
	
	for geo in sunSenStr:
		sunSenTie[geo + 'Tie'] = sunSenTieNc4.variables[geo.upper()][:,:]
	
		sunSenInterp2 = griddata(np.transpose((latTie.ravel(),lonTie.ravel())),\
		sunSenTie[geo + 'Tie'].ravel(),\
		np.transpose((lat.ravel(),lon.ravel())), method='linear')
		
		sunSen[geo] = np.reshape(sunSenInterp2,np.shape(lon))
		
	#%% Read LTOAs
	olciWave = np.loadtxt(path0 + '/olciBandsLambdaMedian')
	Nbands = len(olciWave)
	
	ltoa = np.zeros((Nbands,R,C))
	
	for b in range(0,Nbands):
		numBand = '%02d' % (b+1)
		ltoaNc4b = Dataset(img + '/Oa' + numBand + '_radiance.nc','r')
		ltoa[b,:,:] = ltoaNc4b.variables['Oa' + numBand + '_radiance'][rmin:(rmax+1),cmin:(cmax+1)]
		
	#%% PPE correction
	XPpe = np.ravel(np.transpose(ltoa,(0,2,1)))
	YPpe = [np.roll(XPpe,-2),np.roll(XPpe,-1),np.roll(XPpe,1),np.roll(XPpe,2)]
	YMedianPpe = np.median(YPpe,axis=0)
	YMadPpe = np.median(np.absolute(YPpe-YMedianPpe),axis=0)
	ppeFlag = np.abs(XPpe-YMedianPpe) > np.maximum(10*YMadPpe,0.7)
	XPpeCorr = XPpe*~ppeFlag + YMedianPpe*ppeFlag
	ppeFlag = np.transpose(np.reshape(ppeFlag,(Nbands,C,R)),(0,2,1))
	ltoaCorr = np.transpose(np.reshape(XPpeCorr,(Nbands,C,R)),(0,2,1))
	ltoaCorr[:,[0,1,R-2,R-1],:] = ltoa[:,[0,1,R-2,R-1],:]
	ppeFlag[:,[0,1,R-2,R-1],:] = False

#	ppeflag = np.zeros((Nbands,R,C),dtype=bool)
#	start = time.clock()
#	for b in range(20,21):
#		print(b)
#		ltoab = ltoa[b,:,:]
#		for r in range(2,R-2):
#			for c in range(2,C-2):
#				X = ltoab[r,c]
#				Y = ltoab[[r-2,r-1,r+1,r+2],c]
#				if np.abs(X-np.median(Y)) > max(10*robust.mad(Y),0.7):
#					#ltoa[b,r,c] = np.median(Y)
#					ppeflag[b,r,c] = True
#	#your code here    
#	print time.clock() - start
#	
#	start = time.clock()
#	ker4Med = ppeflag
#	ker4Mad = ppeflag	
#	for b in range(20,21):
#		print(b)
#		for r in range(2,R-2):
#			for c in range(2,C-2):
#				ker4Med[b,r,c] = np.median(ltoa[b,[r-2,r-1,r+1,r+2],c])
#				ker4Mad[b,r,c] = robust.mad(ltoa[b,[r-2,r-1,r+1,r+2],c])
#	ppeflag[b,:,:] = np.abs(ltoa[b,:,:]-ker4Med[b,:,:]) > np.maximum(10*ker4Mad[b,:,:],0.7)
#	#your code here
#	print time.clock() - start
#	#ltoa[b,:,:] = np.median(Y)
#	
#	def mad(arr):
#    """ Median Absolute Deviation: a "Robust" version of standard deviation.
#        Indices variabililty of the sample.
#        https://en.wikipedia.org/wiki/Median_absolute_deviation 
#    """
#    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
#    med = np.median(arr)
#    return np.median(np.abs(arr - med))
	#%% ltoa 2 rhotoa
	rtoa = np.zeros((Nbands,R,C))
	
	for b in range(0,Nbands):
		rtoa[b,:,:] = np.pi/f0thuillier[b]*np.divide(ltoa[b,:,:],np.cos(np.radians(sunSen['sza'])))


	#%%
	d={}
	for x in range(1,10):
		d["string{1}".format(x)]="Hello"

	d['string1']
	
	#%%
	
	print('When you multiply {0} and {1} or {0} and {2}, the result is {0}'.format(0,1,2))
			
	imgList = imgList[0]
